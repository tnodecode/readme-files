# Installation

```bash
## Install Vue CLI
$ npm i -g @vue/cli
$ npm i -g @vue/cli-init

## Install Router
$ npm i vue-router

## Install Vuex
$ npm i vuex

## Create new project
$ vue-init <template> <projectName>

### template options
- simple: index.html + Vue CDN import
- webpack-simple: Basic Webpack Workflow
- webpack: Complex Webpack workflow inc. Testing
- browserify-simple: Browserify workflows
- browserify: Browserify workflows

## - Install node packages
$ cd <projectName>
$ npm install

## Run application
$ npm run dev
```