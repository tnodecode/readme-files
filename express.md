# Installation

```bash
# - Install Vue CLI
$ npm install -g express-generator

# - Create new project
$ express <appName>

# - Install node packages
$ npm install

# Run application
$ npm start
```

# MongoDB

```bash
$ npm i mongodb
```