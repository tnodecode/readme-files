# Installation

```bash
## Install react creator
$ npm i -g create-react-app

## Create a react application
$ create-react-app <projectName>
$ cd <projectName>
$ npm install

## Install router
$ npm i -s react-router-dom

## Install Redux
$ npm i redux react-redux redux-thunk

## Run the app
$ npm start
```

# Redux

```bash
# Installation
$ npm i redux
```

# VS Code Plugins
- ES7 React/Redux/GraphQL/React-Native snippets
