## Config

```bash
# Save login details
$ git config --global user.name "<username>"
$ git config --global user.email "<email>"

# list configuration
$ git config --global --list

## Setup repository on server
$ mkdir <path-to-repository>/<repositoryName>
$ git init --bare

## Initialisation of a git repository

# Initialize local repository root directory
$ git init

# add remote repository (usually called 'origin')
$ git remote add <remoteName> <username>:<password>@<url>:<path-to-repository>/<repositoryName>

# Example with GitLab and 'origin' as remote repository name
$ git remote add origin https://gitlab.com/<username>/<repositoryname>

# Check remote repository
$ git remote -v
```

## Add new files/directories, upload and download (you should execute the following command in this order)

```bash
# Download data (default branch is 'master')
$ git pull <remoteName> <branch>

# Add new files (for adding all new files set 'dir' to '.')
$ git add <dir>

# Commit your changes
$ git commit -m "<commit-message>"

# Git add and commit in one command
$ git commit -am "<commit-message>"

# Upload changes
$ git push <remoteName> master
```

## Branches
 
```bash
# Create branch
$ git branch <new-branch-name>

# show branches
$ git branch

# switch to branch
$ git checkout <branch>

# Create new branch based on the current branch  and switch to the created branch
$ git checkout -b <branch-name>

# Update your branch when the original branch from official repository has been updated
$ git fetch <remoteName>
```

# Cloning
```bash
# Clone repository
$ git clone <repositoryUrl>

# Clone branch only
$ git clone -b <branchName> --single-branch <repositoryUrl>

```

# GitLab
## Build Setup

``` bash

## Git global setup
git config --global user.name "<userName>"
git config --global user.email "<userEmail>"

## Create a new repository
git clone https://gitlab.com/<userName>/<repositoryName>.git
cd <repositoryName>
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

## Existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/<userName>/<repositoryName>.git
git add .
git commit -m "Initial commit"
git push -u origin master

## Existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/<userName>/<repositoryName>.git
git push -u origin --all
git push -u origin --tags

## Create and switch to dev branch
git checkout -b dev
git commit -m "Dev branch initial commit"
git push origin dev
```

