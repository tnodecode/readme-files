# Linux

## Remove PHP 5
$ sudo apt-get purge php5-common

## Remove PHP 7.0
$ sudo apt-get purge php7.0-common

## PHP 7
$ sudo add-apt-repository ppa:ondrej/php
$ sudo apt-get update
$ sudo apt-get install php7.2-cli php7.2-common php7.2-curl php7.2-gd php7.2-json php7.2-mbstring php7.2-mysql php7.2-opcache php7.2-readline php7.2-xml

## Install MongoDB driver
$ sudo apt-get install php-dev pkg-config libssl-dev
$ sudo pecl install mongodb
$ sudo nano /etc/php/7.1/mods-available/mongodb.ini
extension=mongodb.so
$ phpenmod mongodb

# Windows 10
- Lade Zip-Archiv herunter und entpacke es in einem Verzeichnis, z.B. c:\Users\MyUser\Programme\PHP
- Füge den Pfad zu PATH bei den lokalen Umgebungsvariablen hinzu
- Nenne das php.ini-development File in php.ini um
- Passe im ini-File die extension_dir Variable an: extension_dir = "c:\Users\MyUser\Programme\PHP\ext"
- Entferne in den Zeilen, die mit ;extension= anfangen das Semikolon um die benötigten PHP-Module freizuschalten
