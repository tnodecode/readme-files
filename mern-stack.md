# Initialization

```bash
# Install global modules
$ npm i -g express-generator create-react-app nodemon

# Generate express app
$ express myApp
$ cd myApp
$ npm install

# Generate React app
$ npx create-react-app client
```

# Package.json
```json
{
  ...
  "scripts": {
    "start": "node ./bin/www",
    "server": "nodemon",
    "client": "npm start --prefix client",
    "dev": "concurrently \"npm run server\" \"npm run client\"",
    ...
  },
  ...
}
```

Start both the express server and the react dev-server:
```bash
$ npm run dev
```