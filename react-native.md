# Setup
```bash
# Install global Node.js modules
$ npm install -g expo-cli

# Init project
$ expo init my-app

# Go to your app
$ cd my-app/

# Start expo client
$ npm start
```

# Preparing Expo CLI

```bash
# Setting expo ENV variable
$ REACT_NATIVE_PACKAGER_HOSTNAME='192.168.178.<devicePortNumber>'

# Start expo client on LAN
$ expo start --lan

# Now you should be able to open this URL:
http://192.168.178.<devicePortNumber>:19000

# Sometimes you have to craete a QR-Code manually with the URL:
exp://192.168.178.<devicePortNumber>:19000

# Scan this manually generated QR-Code with the Expo App of your smartphone.
```

# React Native CLI
```bash
# Ensure that the following ENV variables are set
ANDOIRD_HOME=C:\Users\<UserName>\AppData\Local\Android\Sdk
JAVA_HOME=C:\Program Files\Java\jdk<JDKVersion>

# Add the following path to the PATH variable
'C:\Users\<UserName>\AppData\Local\Android\Sdk\platform-tools\'

# Restart your terminal / shell after you added / changed your ENV variables

# An Emulator has to be running or a device with USB-Debugging enabled
# has to be connected to your computer

# Install React Native CLI
$ npm i -g react-native-cli

# Create new project
$ react-native init myProject

# Go to project
$ cd myProject

# Run project
$ react-native run-android

# Enable hot reloading
- Press Strg + M on Android, Strg + D on iOS or shake device on real device
- Click 'Enable hot reloading'

```
