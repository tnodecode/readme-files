### Ampps 3.8 installation

# Go to Downloads directory
$ cd /home/<username>/Downloads

# Downlaod Ampps 3.8-x86
$ wget http://s4.softaculous.com/a/ampps/files/Ampps-3.8-x86_64.run

# set execute permission
$ chmod 0755 Ampps-3.8-x86_64.run

# run installation file
$ ./Ampps-3.8-x86_64.run

# change directory
$ cd /usr/local/ampps/

# run Ampps file
$ ./Ampps

# if you get the error: No protocol specified, then log out, select 'Ubuntu on Xorg' in the setting next to the login button and log in again

## Browser

# ampps url
$ http://localhost/ampps

# ampps admin
$ http://localhost/ampps-admin

# phpMyAdmin
$ http://localhost/phpmyadmin