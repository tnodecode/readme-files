# Setup Website
```bash 
$ composer create-project symfony/website-skeleton <name>
```

# Setup API
```
$ composer create-project symfony/skeleton <name>
$ composer req api
```

# JWT (see: https://github.com/lexik/LexikJWTAuthenticationBundle)
```bash
$ composer require lexik/jwt-authentication-bundle

# Register Bundle (in config/bundes.php):
Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle::class => ['all' => true],

# Hinweise zur Installation
- On Read Error: Check chmod of private.pem and public.pem files in config/jwt folder
- in config/security.yaml müssen die firewalls 'login' und 'api' über 'dev' und 'main' stehen, da die Strategien von oben nach unten gelesen werden. Alternativ kann auch das Pattern von 'main' mit einem negativem Lookbehind versehen werden, damit die /api Routes nicht gematched werden.
```

# Composer
```bash
## require devtools: profiler, server, maker
$ composer require symfony/maker-bundle server symfony/profiler-pack symfony/var-dumper doctrine/doctrine-fixtures-bundle --dev
## require server
$ composer require server --dev
## require annotations
$ composer require annotations
## Doctrine
$ composer require doctrine/doctrine-bundle doctrine/orm doctrine/doctrine-migrations-bundle
$ composer req symfony/orm-pack
## Validator
$ composer require symfony/validator
## require sensio extra bundle
$ composer require sensio/framework-extra-bundle
## Translation
$ composer require symfony/translation
## user authentication
$ composer require security security-csrf form
## Swift mailer for sending emails (see https://symfony.com/doc/current/email.html)
$ composer require mailer
## Event Dispatcher
$ composer require symfony/event-dispatcher
## Filesystem
$ composer require symfony/filesystem
## Finder
$ composer require symfony/finder
## Logger
$ composer require logger
## Asset
$ composer require symfony/asset

# Install all at once
$ composer req annotations sensio/framework-extra-bundle security security-csrf form mailer symfony/filesystem symfony/event-dispatcher symfony/translation
```

## Run Project
$ php bin/console server:start 0.0.0.0:8000

## Routes
$ php bin/console debug:router


## Controller
$ php bin/console make:controller <controller-name>

# Doctrine
$ composer require doctrine maker
$ php bin/console doctrine:database:create## Show update SQL code
$ php bin/console doctrine:schema:update --dump-sql## Force update
$ php bin/console doctrine:schema:update --force

## Migration
$ php bin/console doctrine:migrations:diff
$ php bin/console doctrine:migrations:migrate

# Maker Bundle

## Installation
$ composer require symfony/maker-bundle --dev

## Makers
$ php bin/console make:command
$ php bin/console make:controller [<namespace>\\]\<name>
$ php bin/console make:entity [<namespace>\\]\<name>
$ php bin/console make:validator [<namespace>\\]\<name>
$ php bin/console make:voter [<namespace>\\]\<name>

# Translation
$ php bin/console translation:update --dump-messages en
$ php bin/console translation:update --force en

# Easy Admin
```bash
# Installation
$ composer req admin

# Visit Easy Admin
http://<URL>/admin
```

# Unit Tests
see: https://symfony.com/doc/current/testing.html

```bash
# Installation
$ composer req --dev symfony/phpunit-bridge
$ php bin/phpunit
```

Create PHP class in 'tests' folder
```php
class SimpleTest extends TestCase
{
    public function testAddition()
    {
        $this->assertEquals(5, 2+3, 'Five was expected to equal 2+3');
    }
}
```

Run the test
```bash
$ php bin/phpunit
```

# Behat functional tests
```bash
# Installation
$ composer req --dev behat/behat behat/mink behat/mink-extension behat/symfony2-extension behatch/contexts coduo/php-matcher behat/mink-browserkit-driver

# Run the tests
$ ./vendor/bin/behat
```

# External Bundles

## StofDoctrineExtensionsBundle
https://symfony.com/doc/master/bundles/StofDoctrineExtensionsBundle/index.html

## Guzzle HTTP Client (@see https://github.com/guzzle/guzzle)
$ composer require guzzlehttp/guzzle

# AWS buckets (@see https://blog.theodo.fr/2018/06/upload-symfony-aws-s3/)
$ php composer.phar req vich/uploader-bundle && php composer.phar req aws/aws-sdk-php && php composer.phar req knplabs/knp-gaufrette-bundle
