# Coding Tools

###### JSON:
- JSON Editor: https://jsoneditoronline.org/#/
- JSON Schema Generator: https://www.jsonschema.net
- JSON Schema Validator: https://www.jsonschemavalidator.net
- JSON to YAML: https://www.json2yaml.com

###### Regex:
- Regex Tester: https://regex101.com

# Design
- https://www.materialpalette.com
- https://bootswatch.com

# Data

## Locales

### Locale ID List
- https://www.science.co.il/language/Locale-codes.php

### Country Flags
- http://flag-icon-css.lip.is