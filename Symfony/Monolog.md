# Monolog

```bash
# Installation
$ composer req symfony/monolog-bundle
```

## Use Logger
```php
// ...
use Psr\Log\LoggerInterface;

class MyService
{
    // ...
    
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        LoggerInterface $logger
        // ...
    ) {
        $this->logger = $logger;
        // ...
    }

    // ...
    
    public function foo()
    {
        $this->logger->debug('This is a log message');
    }
    
    // ...
}
```

## Debug Logger
```
debug:container log
```

## Creating custom channels
- Create file config/packages/monolog.yaml

```yaml
# config/packages/monolog.yaml
monolog:
    channels: ['my_channel']
```

- You can see the new channel via debug command
```
debug:container monolog.logger.my_channel
```

- add custom channel to your service
```yaml
services:
    # ...
    App\Security\MyService:
        arguments:
            $logger: '@monolog.logger.my_channel'
```
### Logging channels in separate files
This will write log messages of the 'my_channel' channel to the file my_channel.<env>.log
```yaml
monolog:
    handlers:
        # ...
        my_channel:
            type: stream
            level: debug
            path: "%kernel.logs_dir%/my_channel.%kernel.environment%.log"
            channels: ['my_channel']

```


## Get log messages via command line
```
# Get first 100 messages from channel 'app'
$ tail ./var/log/dev.log -n 100 | grep "app"

# Get latest 100 messages from channel 'app'
$ tail ./var/log/dev.log -n 100 | grep "app"
```