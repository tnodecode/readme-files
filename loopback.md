# Installation

```bash
# Install Loopback CLI
$ npm i -g loopback-cli

# Create project
$ lb <projectName>

# Go to project server
$ cd <projectName>

# Start the server
$ npm start
```

# Create a model

```bash
# Create the model
$ lb model

# base class: PersistedModel
# Common model: common
```