Basis commands

### Create new project
composer create-project laravel/laravel <name>

# - Make the following change in the file app/config/app.php
'debug' => env('APP_DEBUG', true),

# - Rename .env.example to .env

# - Generate key
sudo php artisan key:generate

# - Install Javascript modules
npm install

# - Run
php artisan serve

# - Compile Assets
npm run dev

# - Compiles Assets whenever something is changed
npm run watch



### MVC

# Routes
# - List all routes
php artisan route:list

# Controller
# - Create controller
php artisan make:controller <name>

# - Create controller with resource (index, store, create, show, update, destroy, delete)
php artisan make:controller --resource <name>

# Model
# - Create Model with migration
php artisan make:model <name> -m



## Authentication

# - Enable Authentication
php artisan make:authentication



## Forms (see https://laravelcollective.com/docs/5.4/html)

# - Install 
composer require "laravelcollective/html":"^5.4.0"

# - Register in config/app.php
'providers' => [
    // ...
    Collective\Html\HtmlServiceProvider::class,
    // ...
],

'aliases' => [
    // ...
      'Form' => Collective\Html\FormFacade::class,
      'Html' => Collective\Html\HtmlFacade::class,
    // ...
],


### Migration

# - Apply migrations
php artisan migrate

# - Create a migration
php artisan make:migration <migration-name> --create="<table-name>"
