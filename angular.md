## Project setup

# Install Angular CLI
npm nstall -g @angular/cli

# Create new project
ng new <name>

# Run project (default port is 4200)
ng serve


## Include Bootstrap

# Install bootstrap
npm install --save bootstrap

# Edit .angular-cli.json
"styles":[
    ...
    "../node_modules/bootstrap/dist/css/bootstrap.min.css",
    ...
]

## Modules

# Create Module
$ ng g m [<dir>/]<moduleName> [--module=<moduleDir>]

# Example for nested modules
$ ng g m blog --module=app
$ ng g m blog/admin --module=blog
$ ng g m blog/admin/articles --module=blog/admin

## Services

# Create Service
$ ng g s [<dir>/]<serviceName>


## Interfaces

# Create interface
$ ng g i [<dir>/]<interfaceName>

## Components (Replace <name> with the component name)
	

# Create component (app module will be updated automatically)
ng generate component <name>
ng g c <name>
#nested
ng g c [<componentDir>/]<componentName> [--module=[<moduleDir>/]<moduleName>]

# This will create a folder named 'articles' and a 'articles.module.ts' file inside that folder
ng g m 'articles' --module=app

# This generates a component named 'list' in the folder 'articles/list' and adds declaations for thsi component to the 'articles' module
ng g c 'articles/list' --module=articles


# Component template
templateUrl: './<componentName>.component.html'
template: `<html>`

# Component style
styleUrls: ['./<componentName>.component.css']
styles: [`<css>`]

# Component selectors

# Select by element
selector: '<selector>'
<selector></selector>

#Select by attribute
selector: '[<name]'
<div <name> ></div>

# Select by class
selector: '.<selector>'
<div class="<selector>"></div>


## Data binding

#String inerpolation
{{Component property}}
{{<TypeScript code that returns a string>}}

# Property binding
[<html-property>]="<componentclass-property>"
[<html-property>]="!<componentclass-property>" #negotiation of property values

# Event binding
(<event>)="<component-method>($event)"

#Two-way-databinding
[(ngModel)]="<component-property>"


## conditions

# ngIf
<div *ngIf="<component-property"></div>  #Element will ony displayed if <component-property> is true


