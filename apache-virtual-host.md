# Apache: Create virtual host (Ubuntu)

## Before creating a virtual host, make sure that the mod_rewrite module of apache is enabled
sudo a2enmod rewrite

## 1. In this directory the content of the virtual server will be stored
$ sudo mkdir -p /var/www/<url>/public_html

## 2. Current user will be the owner of the new folder
$ sudo chown -R $USER:$USER /var/www/<url>/public_html

## 3. Copy default virtual host config file
$ sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/<url>.conf

Make the following changes in /etc/apache2/sites-available/<url>.conf:

`ServerName <url>
ServerAlias www.<url>
DocumentRoot /var/www/<url>/public_html
<Directory /var/www/<url>>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Require all granted
</Directory>
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>
    RewriteEngine On
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteRule ^(.*)$ /index.php$1 [L,QSA] #This redirects everything to /index.php
</IfModule>
`

## 4. Enable site
$ sudo a2ensite <url>.conf

## 5. Link url to virtual host
$ sudo nano /etc/hosts

Make the following changes in the line 

before:
127.0.0.1   localhost

after:
127.0.0.1   localhost <url>

## 6. Restart apache, so that changes will be applied
$ sudo systemctl restart apache2

## 7. Fill URL with content
Create an index.php or index.html file in /var/www/<url>/public_html/

## 8. Open Browser
Open your browser an call <url>. Your index.php / index.html file will be displayed

