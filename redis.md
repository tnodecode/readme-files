# Redis

## Data Types
- String
- List
- Set (unordered List of Strings)
- Sorted Sets (Non-repeating Collection of Strings)
- Hashes (Key-Value pairs)
- Bitmaps
- Hyperlogs
- Geospatial Indexes

## Start
```bash
# Start
$ redis-cli
```

# Basic commands
```bash
# Set and Get (variables are case-sensitive -> "foo" is not "Foo")
$ SET foo 100
OK
# Set multiple values
$ MSET key1 "Hello" key2 "World"
$ GET foo
"100"
# Get multiple values
$ MGET key1 key2
1) "Hello"
2) "World"
# Append a value to an existing value
$ SET foo "Hello"
$ APPEND foo "#"
$ GET foo
"Hello#"

# Check if something exists
$ EXISTS foo
(integer) 1
$ EXISTS bar
(integer) 0

# Expiration after x Seconds
$ EXPIRE foo 60
# Get time left for expiring variable
$ TTL foo
# Set value with expiration in one step
$ SETEX foo 60 "Hello World"
# Take away expiration
$ PERSIST foo
# Rename keys
$ RENAME foo bar

# Increment
INCR foo
"101"

# Delete single item
$ DEL foo

# Delete all
$ FLUSHALL
```

## Lists
```bash
# Create List (or add item to list at the beginning)
$ LPUSH people "John"

# Add item to list at the end (right)
$ RPUSH people "Jane"

# Get list items
$ LRANGE <startIndex> <endIndex>
# Show all items
$ LRANGE 0 -1

# List length
$ LLEN people

# Remove and display first item in the list
$ LPOP people

# Remove and display last item in the list
$ RPOP people

# Insert item in the middle of the list
$ LINSERT people BEFORE "Jane" "Mike"
```

# Sets
```bash
# Create Set (or add items to a set)
$ SADD cars "Ford"

# Get all members of a Set
$ SMEMBERS cars

# Count members in a set
$ SCARD cars

# Check if set contains element
$ SISMEMBER cars "Ford"

# Remove member from a Set
$ SREM cars "Ford"
```

## Sorted Sets

```bash
# Create Sorted Set (or add items to a Sorted Set)
$ ZADD marks 12 "John"
$ ZADD marks 15 "Jane"
$ ZADD marks 11 "Jen"

# Get the rank of an item
$ ZRANK marks "Jane"
(integer) 2
$ ZRANK marks "John"
(integer) 1
$ ZRANK marks "Jen"
(integer) 0

# Get all members of a Sorted Set (ordered by their rank)
$ ZRANGE marks 0 -1
```

## Hashes

```bash
# Create a key-value-pair in a hash
$ HSET user:john name "John Doe"
$ HSET user:john email "john.doe@inet.com"

# Get a value
$ HGET user:john name
"John Doe"
$ HGET user:john email
"john.doe@inet.com"

# Get all keys
$ HGETALL user:john
1) "name"
2) "John Doe"
3) "email"
4) "john.doe@inet.com"

# Set multiple values
$ HMSET user:john name "John Doe" email "john.doe@inet.com"

# Get keys of Hash
$ HKEYS user:john
1) "name"
2) "email"

# Get values of Hash
$ HVALS user:john
1) "John Doe"
2) "john.doe@inet.com"

# Delete key-value pair of Hash
$ HDEL user:john email

# Get number of key-value pairs of Hash
$ HLEN user:john
```

## Persist data on disk

Data is saved at `/var/lib/redis` 

```bash
# Save data
$ SAVE

# Save every 60 seconds if at least 1000 keys have changed
$ SAVE 60 1000
```