# Installation

```bash
$ npm i redux
```

# Usage

## Basic Usage outside of React
```javascript
// react-demo.js

const redux = require('redux');
const createStore = redux.createStore;

// Initial State
const initialState = {
  counter: 0,
}

// Reducer
const rootReducer = (state = initialState, action) => {
  if (action.type === 'INC_COUNTER') {
    return {
      ...state,
      counter: state.counter + 1
    };
  }
  if (action.type === 'ADD_COUNTER') {
    return {
      ...state,
      counter: state.counter + action.value
    };
  }
  return state;
}

// Store
const store = createStore(rootReducer);
console.log('[Initial State]', store.getState());

// Subscription
store.subscribe(() => {
  console.log('[Subscription]', store.getState());
});

// Dispatching Action
store.dispatch({ type: 'INC_COUNTER' });
store.dispatch({ type: 'ADD_COUNTER', value: 10 });

console.log('[Final State]', store.getState());
```

Run this code via terminal should produce the following output:

```bash
$ node react-demo.js

[Initial State] { counter: 0 }
[Subscription] { counter: 1 }
[Subscription] { counter: 11 }
[Final State] { counter: 11 }
```

# Basic usage inside of react
```bash
$ npm i react-redux
```