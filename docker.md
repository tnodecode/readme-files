# Images

## Enable doocker daemon in Windows 10 by ...
- rightclick on the docker symbol in the Windows 10 taskbar
- Click 'Settings'
- Check 'Enable daemon on tcp:// ...'

## Version
$ docker version

## More information about docker (number running / paused / stopped of containers)
$ docker info

## Save Login Information (in Windows only works with cmd console, not with Git bash)
$ docker login --username <username> --password <password>

## more secure login
$ docker login --username --password-stdin

# Build an image

## Create an image with the name 'hello-world'
$ docker build -t hello-world .

## Pull image from Dockerhub
$ docker pull <imageName>

## Show all built images
$ docker images
$ docker image -a

## Remove image
$ docker rm <imageID> # You can enter the first characters of the ID and docker will know which container is meant

# Containers

## Run a container based on your created image 'hello-world'
$ docker run hello-world

## forward port 80 from the host to port 80 in the container
$ docker run -p 80:80 <imageName>

## Mount local directory <localDir> into <containerDir> in the container
$ docker run -v <localDir>:<containerDir> <imageName>

## Show running containers
$ docker ps

## Show all containers (running and stopped)
$ docker ps -a

## Stop all containers
$ docker stop $(docker ps -a -q)

## Remove single container
$ docker rm <containerName/containerID> # You can enter the first characters of the ID and docker will know which container is meant

## Remove all containers
$ docker rm $(docker ps -a -q)

## Enter running container

## Bash
docker exec -it <containerName> bash

# Clean up

## clean up any resources — images, containers, volumes, and networks — that are dangling (not associated with a container)
$ docker system prune

## additionally remove any stopped containers and all unused images (not just dangling images)
$ docker system prune -a

## remove all networks
$ docker network rm $(docker network ls -q)


# Docker Compose

## list all servcies
$ docker-compose ps

## Run services
$ docker-compose up -d

## Restart containers
$ docker-compose restart

## Stop containers
$ docker-compose stop
### stop only one. container it still running!
$ docker-compose stop <containerName>

## Rebuild single container
$ docker-compose build --no-cache <containerName>

## this will remove the docker container permanently 
docker-compose rm <containerName>

## Destroy containers
$ docker-compose down