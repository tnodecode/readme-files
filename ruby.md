### Installation

# - Check if Java is installed
java -showversion

# - Install Neo4j
$ wget --no-check-certificate -O - https://debian.neo4j.org/neotechnology.gpg.key | sudo apt-key add -
$ echo 'deb http://debian.neo4j.org/repo stable/' | sudo tee /etc/apt/sources.list.d/neo4j.list
$ sudo mv /tmp/neo4j.list /etc/apt/sources.list.d
$ sudo apt update
$ sudo apt install neo4j=3.1.4
$ sudo service neo4j restart

# - Now open http://localhost:7474/browser/